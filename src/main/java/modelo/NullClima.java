package modelo;

import modelo.Lugar;

public class NullClima implements Clima{
	
	
	protected Fecha fecha;
	protected Lugar lugar;
	protected String temperatura;
	protected String maxtemperatura;
	protected String condiciones;
	
	public int getDia()
	{
		return 0;
	}
	
	
	public int getMes()
	{
		return 0;
	}
	
	public int getAnio()
	{
		return 0;
	}
	
	
	public NullClima ()
	{
		lugar=new Lugar("","","");
	}
	public String getTemperatura() {
		return "";
	}
	
	public String getMaxTemperatura() {
		return "";
	}
	

	public String getCondiciones() {
		return "";
	}

	public Lugar getLugar() {
		return new Lugar("","","");
	}
	@Override
	public String toString() {
		return "JTiempo [Pais=" + lugar.getPais() + ", Provincia=" + lugar.getProvincia() + ", Localidad=" + lugar.getLocalidad()
			 + ", temp=" + temperatura+ ", maxt=" + maxtemperatura
				+", conditions=" + condiciones + "]";
	}

}
