package User3;

import org.apache.http.client.ClientProtocolException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import modelo.Cache;
import modelo.Clima;
import modelo.ControladorDeCache;
import modelo.Fecha;
import modelo.Lugar;
import modelo.NullClima;
import modelo.PoliticaCache1;
import modelo.RealClima;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
public class  CA3 {
	 

    Cache cache=Cache.getInstancia();
	
	    ControladorDeCache cont_cache=new ControladorDeCache(new PoliticaCache1());
	    Fecha f= new Fecha(10,4,2021);
		
	    @Before public void antes()
	{
	    	
		  Lugar l=new Lugar( "Argentina","Buenos Aires","Garin");
         Clima t=new RealClima(f, l, "25.76", "25.76", "light rain");
         Calendar hoy=Calendar.getInstance();
			Date fechaactual=new Date();
			SimpleDateFormat simple=new SimpleDateFormat("dd/MM/YYYY");
			String fechastring=simple.format(fechaactual);
			

			String[] fecha = fechastring.split("/");
			int dia = Integer.parseInt(fecha[0].toString()); 
			int mes = Integer.parseInt(fecha[1].toString()); 
			int anio=Integer.parseInt(fecha[2].toString()); 

			
			f=new Fecha(dia,mes,anio);
         String cacheKey=l.getPais()+l.getProvincia()+l.getLocalidad()+"|"+f.getAnio()+f.getMes()+f.getDia();
   	  
         cont_cache.agregar(cacheKey, t);
          
          
          
          l=new Lugar( "Argentina","Buenos Aires","Don Torcuato");
         
          t=new RealClima(f, l, "23", "23", "light rain");
          cacheKey=l.getPais()+l.getProvincia()+l.getLocalidad()+"|"+f.getAnio()+f.getMes()+f.getDia();
       	  cont_cache.agregar(cacheKey, t);
          
                   


          l=new Lugar( "Argentina","Buenos Aires","Mar del Plata");
      
          t=new RealClima(f, l, "16", "16", "clouds");
          
          cacheKey=l.getPais()+l.getProvincia()+l.getLocalidad()+"|"+f.getAnio()+f.getMes()+f.getDia();
       	  cont_cache.agregar(cacheKey, t);
   
                   

          
	}
	//luego de 3 elementos se vacia la cache y vuelve a iniciarse
    @Test public void Cache_vaciar() throws ClientProtocolException, URISyntaxException, IOException {
    	 
        
        Lugar l=new Lugar( "Argentina","Buenos Aires","Miramar");
       
         Clima  t=new RealClima();
         
         t=new RealClima(f, l, "16", "16", "clouds");

         String cacheKey=l.getPais()+l.getProvincia()+l.getLocalidad()+"|"+f.getAnio()+f.getMes()+f.getDia();
      	  
         cont_cache.agregar(cacheKey, t);
         
         assertTrue(cache.tamanio()==1);
    		  
    }
    
    @After public void despues()
    {

        cache.borrarTodo();
    }
}
