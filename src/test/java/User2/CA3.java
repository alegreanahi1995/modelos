package User2;
import org.apache.http.client.ClientProtocolException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import modelo.ControladordeServicio;
import modelo.Fecha;
//import modelo.ApiPrueba;
import modelo.Lugar;
import modelo.NullClima;
import modelo.ServicioClima;
import modelo.ServicioNull;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
public class  CA3 {

		ControladordeServicio cont_servicios=new ControladordeServicio();
	   


	  @Test public void Consulta_Proveedor_Unico_NullObject() throws ClientProtocolException, URISyntaxException, IOException, InterruptedException {
	   
			assertTrue(cont_servicios.getServicios().size()==1);
			List<ServicioClima>lista=cont_servicios.getServicios();
			assertTrue(lista.get(0) instanceof ServicioNull); 
			 Lugar lugar=new Lugar("Argentina","Buenos Aires","Garin");
			    
			    Calendar hoy=Calendar.getInstance();
				Date fechaactual=new Date();
				SimpleDateFormat simple=new SimpleDateFormat("dd/MM/YYYY");
				String fechastring=simple.format(fechaactual);
				

				String[] fecha = fechastring.split("/");
				int dia = Integer.parseInt(fecha[0].toString()); 
				int mes = Integer.parseInt(fecha[1].toString()); 
				int anio=Integer.parseInt(fecha[2].toString()); 

				hoy.set(anio, mes,dia);
				
				Fecha f=new Fecha(dia,mes,anio);
			assertTrue(lista.get(0).getTiempo(lugar, f) instanceof NullClima);
			
	  }



}
