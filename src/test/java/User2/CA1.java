package User2;

import org.apache.http.client.ClientProtocolException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import modelo.Clima;
import modelo.ControladordeServicio;
import modelo.Fecha;
//import modelo.ApiPrueba;
import modelo.Lugar;
import modelo.NullClima;
import modelo.ServicioClima;
import servicios.ServicioPrueba;
import servicios.ServicioPruebaNoFunciona;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
public class  CA1 {
	

	ControladordeServicio contro_servicios=new ControladordeServicio();
	   

    @Test public void Consulta_Proveedor_Contiene() throws ClientProtocolException, URISyntaxException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {
    

    	 List<ServicioClima> lista=new ArrayList<ServicioClima>();
  	   	
	    lista.add(new ServicioPrueba());
	    contro_servicios.cargarServicios(lista);
	    Lugar lugar=new Lugar("Argentina","Buenos Aires","Garin");
	    
	    Calendar hoy=Calendar.getInstance();
		Date fechaactual=new Date();
		SimpleDateFormat simple=new SimpleDateFormat("dd/MM/YYYY");
		String fechastring=simple.format(fechaactual);
		

		String[] fecha = fechastring.split("/");
		int dia = Integer.parseInt(fecha[0].toString()); 
		int mes = Integer.parseInt(fecha[1].toString()); 
		int anio=Integer.parseInt(fecha[2].toString()); 

		hoy.set(anio, mes,dia);
		
		Fecha f=new Fecha(dia,mes,anio);
	    Clima clima=contro_servicios.getServicios().get(0).getTiempo(lugar, f);
	    
	    assertTrue(clima.getMaxTemperatura().compareTo("20 grados")==0);
	    assertTrue(clima.getCondiciones().compareTo("Nublado")==0);
	
    }
    


}
