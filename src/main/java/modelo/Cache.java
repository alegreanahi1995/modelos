package modelo;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Cache {

	  private Map<String, Object> cache = Collections.synchronizedMap(new HashMap<String, Object>());

      private static Object monitor = new Object();
      private static Cache instancia;
	  
	  public void agregar(String clave,Object value) {
   	   
   	 
     		cache.put(clave, value);
     	  
      }
      
	  public boolean contiene(String clave) {
          return cache.containsKey(clave);
        
      }
 
	  public Object devolverValor(String clave) {
		  return cache.get(clave);
	  }

      public void borrarTodo() {
          cache.clear();
      }
      
      
      public void borrarUnElemento(String clave)
      {

  		cache.remove(clave);
      }
      
      public static Cache getInstancia() {
          if (instancia == null) {
              synchronized (monitor) {
                  if (instancia == null) {
                      instancia = new Cache();
                  }
              }
          }
          return instancia;
      }

      public Map<String, Object> devolvertodosloselementos()
      {
    	  return cache;
      }
      
      public int tamanio()
      {
   	   return cache.size();
      }
      
     
	
}
