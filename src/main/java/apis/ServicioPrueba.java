package apis;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.client.ClientProtocolException;

import modelo.Clima;
import modelo.Fecha;
import modelo.Lugar;
import modelo.NullClima;
import modelo.RealClima;
import modelo.ServicioClima;

public class ServicioPrueba implements ServicioClima {

	
		    String API_KEY = "988ebbfc0bc3f8b2039b9ec2f4f53b33";
		    String nombre;
		    String url;
		    public ServicioPrueba()
		    {
		    	this.nombre="ApiPrueba";
		    	this.url=" www.openweathermap.org";
		    }
		    
		    public String getEstado()
		    {
		    	return "Con conexion";
		    }

		    
		   public Clima getTiempo(Lugar l, Fecha fecha) throws ClientProtocolException, URISyntaxException, IOException {

			
				int dia,mes,anio;
				Lugar lugar=new Lugar("Argentina","Buenos Aires","Garin");
			
				Calendar cal=Calendar.getInstance();
				Date data=cal.getTime();
			    dia=cal.get(Calendar.DATE);
		  		mes=cal.get(Calendar.MONTH)+1;
		  		anio=cal.get(Calendar.YEAR);
		  		Fecha f=new Fecha(dia,mes,anio);
		  		
		  		RealClima t=new RealClima(f,lugar,"20 grados","20 grados","Nublado");
				
				Calendar calendario	=Calendar.getInstance();
				Date fechaactual=new Date();
				SimpleDateFormat simple=new SimpleDateFormat("dd/MM/YYYY");
				String fechastring=simple.format(fechaactual);
				
				String[] fechavector = fechastring.split("\\/");
				int diaactual = Integer.parseInt(fechavector[0].toString()); 
				 int mesactual = Integer.parseInt(fechavector[1].toString()); 
				int anioactual=Integer.parseInt(fechavector[2].toString()); 
			
				calendario.set(anio, mes,dia);

				if(dia==diaactual && mes==mesactual && anio==anioactual
						&& l.getLocalidad().compareTo("Garin")==0
						&& l.getProvincia().compareTo("Buenos Aires")==0
						&& l.getPais().compareTo("Argentina")==0)
					return t;
				
				
				return new NullClima();
		    	}



	
		

}
