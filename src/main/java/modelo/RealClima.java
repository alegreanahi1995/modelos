package modelo;
import java.sql.Date;
import java.text.DateFormat;

public class RealClima  implements Clima {
	
	protected Fecha fecha;
	protected Lugar lugar;
	protected String temperatura;
	protected String maxtemperatura;
	protected String condiciones;
	
	
	public RealClima(Fecha fecha,Lugar lugar,String temp,String maximatemp, String condiciones)
	{
		
		this.fecha=fecha;
		this.lugar=lugar;
		this.temperatura=temp;
		this.maxtemperatura=maximatemp;
		this.condiciones=condiciones;
	}
	public int getDia()
	{
		return this.fecha.getDia();
	}
	
	
	public int getMes()
	{
		return this.fecha.getMes();
	}
	
	public int getAnio()
	{
		return this.fecha.getAnio();
	}
	
	public RealClima ()
	{
		lugar=new Lugar("","","");
	}

	public String getTemperatura() {
		return temperatura;
	}
	
	public String getMaxTemperatura() {
		return maxtemperatura;
	}
	
	public String getCondiciones() {
		return condiciones;
	}
	

	
	public Lugar getLugar() {
		return new Lugar("","","");
	}
	
	@Override
	public String toString() {
		return "JTiempo [Pais=" + lugar.getPais() + ", Provincia=" + lugar.getProvincia() + ", Localidad=" + lugar.getLocalidad()
			 + ", temp=" + temperatura + ", maxt=" + maxtemperatura
				+", conditions=" + condiciones + "]";
	}
	

}
