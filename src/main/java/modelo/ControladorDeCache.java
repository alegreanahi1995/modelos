package modelo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ControladorDeCache {

	
	 private PoliticaCache poli_cache;
	 
	 public ControladorDeCache(PoliticaCache poli_cache)
	 {
		 this.poli_cache=poli_cache;
	 }
	 private void eliminarelementosviejos() {
			
		    Calendar hoy=Calendar.getInstance();
	   		Date fechaactual=new Date();
			SimpleDateFormat simple=new SimpleDateFormat("dd/MM/YYYY");
			String fechastring=simple.format(fechaactual);
			

			String[] fecha = fechastring.split("/");
			int dia = Integer.parseInt(fecha[0].toString()); 
			int mes = Integer.parseInt(fecha[1].toString()); 
			int anio=Integer.parseInt(fecha[2].toString()); 
		
			hoy.set(anio, mes,dia);
				
			Cache cache=Cache.getInstancia();

				
	for (Map.Entry<String, Object> entry : cache.devolvertodosloselementos().entrySet()) {

		String[] parts = entry.getKey().split("\\|");
		String cadena = parts[0];
		
		if(entry.getKey().compareTo(cadena+"|"+anio+mes+dia)!=0)
		{
			cache.borrarUnElemento(entry.getKey());
		}
		
	}
	 }
	  public void agregar(String clave,Object valor) {
   	   
   	 
		  this.eliminarelementosviejos();
		  if(!this.contiene(clave))
	  {
			  poli_cache.vaciarcache();
			  Cache.getInstancia().agregar(clave, valor);
	  }
     	  
      }
      
	  public boolean contiene(String clave) {
          return  Cache.getInstancia().contiene(clave);
        
      }
	  
	  public Clima getTiempo(Lugar l, Fecha f)
	  {
		  String clave=l.getPais()+l.getProvincia()+l.getLocalidad()+"|"+f.getAnio()+f.getMes()+f.getDia();
	
		  this.eliminarelementosviejos();
		  if (!this.contiene(clave))
		  {
			  return new NullClima();
		  }
		  return (RealClima) Cache.getInstancia().devolverValor(clave);
		 
		  

	  }
 
  
 
}
