package User2;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import modelo.ControladordeServicio;
import modelo.Lugar;
import modelo.ServicioClima;
import modelo.ServicioNull;
import servicios.ServicioPrueba;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.http.client.ClientProtocolException;
public class CA6{

	ControladordeServicio cont_servicios=new ControladordeServicio();
	   


	  @Test public void Agregar_Proveedor() throws ClientProtocolException, URISyntaxException, IOException, InterruptedException {
	   
			assertTrue(cont_servicios.getServicios().size()==1);
			List<ServicioClima>lista=cont_servicios.getServicios();
			assertTrue(lista.get(0) instanceof ServicioNull); 
			
			List<ServicioClima>set=new ArrayList<ServicioClima>();
			
			lista.add(new ServicioPrueba());
			cont_servicios.cargarServicios(set);
			assertTrue(cont_servicios.getServicios().size()==1);
	
	}
 

}
