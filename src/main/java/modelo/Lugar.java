package modelo;

public class Lugar {

	
	private String Pais;
	private String Provincia;
	private String Localidad;
	
	public Lugar(String pais,String provincia, String localidad)
	{
		Pais=pais;
		Provincia=provincia;
		Localidad=localidad;
	}
	public String getPais() {
		return Pais;
	}
	public void setPais(String pais) {
		Pais = pais;
	}
	public String getProvincia() {
		return Provincia;
	}
	public void setProvincia(String provincia) {
		Provincia = provincia;
	}
	public String getLocalidad() {
		return Localidad;
	}
	public void setLocalidad(String localidad) {
		Localidad = localidad;
	}
	
}
