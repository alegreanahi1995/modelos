package modelo;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import org.apache.http.client.ClientProtocolException;


public class ControladordeServicio {

static List<ServicioClima> listadeservicios=new ArrayList<ServicioClima>();;
static String ruta;
public ControladordeServicio (String ruta)
{

	this.ruta=ruta;
	listadeservicios= new ArrayList<ServicioClima>();
			List<ServicioClima> objeto;
			try {
		
				  File file1 = new File(ruta);

				    //Checks if file1 exists
				    if(file1.exists() && file1.isDirectory()){
				        System.out.println(file1 + " Exists");
				    }else{
				        System.out.println(file1 + " Does not exists");
				    
				    return;}
				objeto = buscarServicios(ruta);
				

				
				this.cargarServicios(objeto);

			} catch (InstantiationException | IllegalAccessException e) {
				// TODO Auto-generated catch block

				e.printStackTrace();
			}
				
}


public ControladordeServicio()
{


	listadeservicios= new ArrayList<ServicioClima>();
}
public void cargarServicios(List<ServicioClima> lista)
{
	

	for (ServicioClima cadena : lista) {
		
		listadeservicios.add(cadena);
		 }
}

private static List<ServicioClima> buscarServicios(String path) throws InstantiationException, IllegalAccessException {
			List<ServicioClima> result = new ArrayList<ServicioClima>();

			for (File f : new File(path).listFiles()) {

				ServicioClima i;
				try {

					i = (ServicioClima) Class.forName("servicios."+f.getName().substring(0, f.getName().length()-4)).newInstance();
				
					result.add(i);
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}

			return result;
			}

		
		
		
		public static List<ServicioClima> getServicios()
		{
			
			if(listadeservicios.size()!=0)
			return listadeservicios;
			
			List<ServicioClima> lista=new ArrayList<ServicioClima>();
			lista.add(new ServicioNull());
			return lista;
			
		}


		public static ServicioClima primerServicioFuncionayContiene(Lugar l,Fecha f) throws URISyntaxException
		{

			List<ServicioClima> serviciosfuncionan=new ArrayList<ServicioClima>();
			for(ServicioClima c: listadeservicios)
			{

				try {
					if( c.getEstado().compareTo("Con conexion")==0)
					{
						if(!(c.getTiempo(l,f) instanceof NullClima))
						{

							serviciosfuncionan.add(c);
						}
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(serviciosfuncionan.size()==0)
				return new ServicioNull();
			else
			{

				int tamanio=serviciosfuncionan.size();
				Random r=new Random();
				int valor=r.nextInt(tamanio);
				return serviciosfuncionan.get(valor);
			
			}
		}
			
			public static List<ServicioClima> serviciosQueFuncionan() throws URISyntaxException
			{
				List<ServicioClima> serviciosfuncionan=new ArrayList<ServicioClima>();
				for(ServicioClima c: listadeservicios)
				{

					try {
						if( c.getEstado().compareTo("Con conexion")==0)
						{
							serviciosfuncionan.add(c);
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				return serviciosfuncionan;
				
			}

			

				public static List<ServicioClima> serviciosqueContienen(Lugar l,Fecha f) throws URISyntaxException
				{
					List<ServicioClima> serviciosfuncionan=new ArrayList<ServicioClima>();
					for(ServicioClima c: listadeservicios)
					{

						try {
							if(!(c.getTiempo(l,f) instanceof NullClima))
					
								serviciosfuncionan.add(c);
							
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					return serviciosfuncionan;
					
				}
}
