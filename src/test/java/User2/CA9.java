package User2;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import modelo.Clima;
import modelo.ControladordeServicio;
import modelo.Fecha;
import modelo.Lugar;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;

import modelo.ServicioClima;
import servicios.ServicioNoConoceUbicacion;
import servicios.ServicioPrueba;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import modelo.NullClima;

import org.apache.http.client.ClientProtocolException;
public class CA9{
	    ControladordeServicio cont_servicios=new ControladordeServicio();

 @Test public void Consulta_NingunProveedorConoceLaUbicacion() throws ClientProtocolException, URISyntaxException, IOException, InterruptedException, InstantiationException, IllegalAccessException, ClassNotFoundException {

	 List<ServicioClima> lista=new ArrayList<ServicioClima>();
	   lista.add(new ServicioNoConoceUbicacion());
	    lista.add(new ServicioNoConoceUbicacion());
		cont_servicios.cargarServicios(lista);
	    Lugar lugar=new Lugar("Argentina","Buenos Aires","Garin");
	    
	    Calendar hoy=Calendar.getInstance();
		Date fechaactual=new Date();
		SimpleDateFormat simple=new SimpleDateFormat("dd/MM/YYYY");
		String fechastring=simple.format(fechaactual);
		

		String[] fecha = fechastring.split("/");
		int dia = Integer.parseInt(fecha[0].toString()); 
		int mes = Integer.parseInt(fecha[1].toString()); 
		int anio=Integer.parseInt(fecha[2].toString()); 

		hoy.set(anio, mes,dia);
		
		Fecha f=new Fecha(dia,mes,anio);
	    Clima clima=cont_servicios.primerServicioFuncionayContiene(lugar, f).getTiempo(lugar, f);
	    
	    assertTrue(clima instanceof NullClima);
		
}
 
}
