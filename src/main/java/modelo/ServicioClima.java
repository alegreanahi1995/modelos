package modelo;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

public interface ServicioClima {


		public Clima getTiempo(Lugar l,Fecha f) throws ClientProtocolException, URISyntaxException, IOException;
		
		public String getEstado()throws IOException, InterruptedException;

	

}
