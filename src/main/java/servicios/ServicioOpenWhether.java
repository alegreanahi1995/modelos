package servicios;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import modelo.Clima;
import modelo.Fecha;
import modelo.Lugar;
import modelo.NullClima;
import modelo.RealClima;
import modelo.ServicioClima;


public class ServicioOpenWhether implements ServicioClima {

       String API_KEY = "988ebbfc0bc3f8b2039b9ec2f4f53b33";
	   String nombre;
	   String url;
	    	
	    public ServicioOpenWhether()
	    {
	    	this.nombre="ApiOpenWhether";
	    	this.url=" www.openweathermap.org";
	    }
	   public Clima getTiempo(Lugar l, Fecha fecha) throws ClientProtocolException, URISyntaxException, IOException {

			
			return JsonClimaOpenWhater(fecha,l.getLocalidad(),l.getProvincia(),l.getPais());
			
			
			
			
	   }


	 private static Map<String,Object> jsonToMap(String str){
	        Map<String,Object> map = new Gson().fromJson(str,new 
	    TypeToken<HashMap<String,Object>> () {}.getType());
	        return map;
	 }
	    
	private Clima JsonClimaOpenWhater(Fecha fecha,String localidad, String provincia, String pais) {
	    StringBuilder result = new StringBuilder();
	   // String LOCATION = pais + "," + provincia + ", BUE";

	    String LOCATION = pais + "," + provincia ;
	    String urlString = "http://api.openweathermap.org/data/2.5/weather?q=" + LOCATION + "&appid=" + API_KEY + "&units=metric";
	    
		  try{		    
		        URL url = new URL(urlString);
		        URLConnection conn = url.openConnection();
		        BufferedReader rd = new BufferedReader(new InputStreamReader (conn.getInputStream()));
		        String line;
		        
		        while ((line = rd.readLine()) != null){
		            result.append(line);
		        }

		        rd.close();
            
		    }catch (IOException e){
		        System.out.println(e.getMessage());
		        return new NullClima();
		    }
			 JSONObject jsonWeatherForecast = null;
			 jsonWeatherForecast = new JSONObject(result.toString());
			 
			 JSONArray weather=jsonWeatherForecast.getJSONArray("weather");
			 JSONObject forecastValue = weather.getJSONObject(0);
			 
			 
			 Map<String, Object > respMap = jsonToMap (result.toString());
		     Map<String, Object > mainMap = jsonToMap (respMap.get("main").toString());
	
		     String jsonComplejo="{'Pais':'" + pais + "', 'Provincia':'" + provincia + "', 'Localidad':'" + localidad
		 		+ "', 'temp':'" + mainMap.get("temp").toString() + "', 'maxt':'" + mainMap.get("temp_max").toString()
		 				+"', 'conditions':'" + forecastValue.getString("description") + "' }";
		 		  System.out.print(result.toString());
	
		 		 RealClima t;
					if(jsonComplejo.compareTo("")!=0)
					{
						Gson gson=new Gson();
						
					     
						t=new RealClima(fecha,new Lugar(pais,provincia,localidad),mainMap.get("temp").toString() ,mainMap.get("temp_max").toString()
							 				, forecastValue.getString("description"));
						
						return t;
					}
					
					Clima n=new NullClima();
					return n;
		  
          

	}
	
	@Override
	public String getEstado() throws IOException, InterruptedException {
		Runtime runtime = Runtime.getRuntime();
		Process proc = runtime.exec("ping "+url); 

		int mPingResult = proc .waitFor();
		if(mPingResult == 0){
		       return "Con conexion";
		}else{
		       return "Sin conexion";
		}
	}
	 

	

}