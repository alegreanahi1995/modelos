package modelo;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Calendar;

import org.apache.http.client.ClientProtocolException;



public interface ConsultadorClima {


	   public Clima getTiempo(Lugar l, Fecha f) throws ClientProtocolException, URISyntaxException, IOException ;
	
}
