package User3;


import org.apache.http.client.ClientProtocolException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import modelo.Cache;
import modelo.Clima;
import modelo.ControladorDeCache;
import modelo.Fecha;
import modelo.Lugar;
import modelo.NullClima;
import modelo.PoliticaCache1;
import modelo.RealClima;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Calendar;
public class  CA2 {

	

  Cache cache=Cache.getInstancia();
  Fecha f= new Fecha(10,4,2021);
	@Before public void antes()
	{
		  
		cache.borrarTodo();
		   assertTrue(Cache.getInstancia().tamanio()==0);
	      
		
	}
  @Test public void Cache_devolverTiempoNoExistente() throws ClientProtocolException, URISyntaxException, IOException {
   
  	ControladorDeCache consul_cache=new ControladorDeCache(new PoliticaCache1());
  	  
   assertTrue(consul_cache.getTiempo(new Lugar("Argentina","Buenos Aires","Garin"), f) instanceof NullClima);
    	
     
   }
  

  
  
 }
