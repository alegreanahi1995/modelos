package modelo;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Calendar;

import org.apache.http.client.ClientProtocolException;

public class ConsultadorDeCache implements ConsultadorClima{


	ConsultadorDeServicio consserv;
	public ConsultadorDeCache(ConsultadorDeServicio consserv)
	{
		this.consserv=consserv;
	}
	public Clima getTiempo(Lugar l, Fecha f) throws ClientProtocolException, URISyntaxException, IOException {
		
		ControladorDeCache cont_cache=new ControladorDeCache(new PoliticaCache1());
		String cacheKey=l.getPais()+l.getProvincia()+l.getLocalidad()+f.getAnio()+"|"+f.getMes()+f.getDia();
		
		if(cont_cache.contiene(cacheKey))
		{
			return cont_cache.getTiempo(l, f);
			 
		}
		
		else 
		{
			Clima serv=consserv.getTiempo(l, f);
			cont_cache.agregar(cacheKey,serv);
			return serv;
		}
	}

}
