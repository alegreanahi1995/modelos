package User3;


import org.apache.http.client.ClientProtocolException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import modelo.Cache;
import modelo.Clima;
import modelo.ConsultadorDeCache;
import modelo.ControladorDeCache;
import modelo.Fecha;
import modelo.Lugar;
import modelo.PoliticaCache1;
import modelo.RealClima;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
public class  CA1 {
	ControladorDeCache consul_cache=new ControladorDeCache(new PoliticaCache1());
    Cache cache=Cache.getInstancia();
    Fecha f=new Fecha(10,4,2021);
	@Before public void antes()
	{
		  Lugar l=new Lugar( "Argentina","Buenos Aires","Garin");
	  
	      assertTrue(Cache.getInstancia().tamanio()==0);
	     Calendar hoy=Calendar.getInstance();
			Date fechaactual=new Date();
			SimpleDateFormat simple=new SimpleDateFormat("dd/MM/YYYY");
			String fechastring=simple.format(fechaactual);
			

			String[] fecha = fechastring.split("/");
			int dia = Integer.parseInt(fecha[0].toString()); 
			int mes = Integer.parseInt(fecha[1].toString()); 
			int anio=Integer.parseInt(fecha[2].toString()); 

			
			f=new Fecha(dia,mes,anio);
			
			
			 Clima t=new RealClima(f, l, "25.76", "25.76", "light rain");
			 String cacheKey=l.getPais()+l.getProvincia()+l.getLocalidad()+"|"+f.getAnio()+f.getMes()+f.getDia();
		   	  
			 consul_cache.agregar(cacheKey,t);
			 
		
	}
    @Test public void Cache_devolverTiempoExistente() throws ClientProtocolException, URISyntaxException, IOException {
    assertTrue(consul_cache.getTiempo(new Lugar("Argentina","Buenos Aires","Garin") ,f).getCondiciones().equals("light rain"));
      
     assertTrue(consul_cache.getTiempo(new Lugar("Argentina","Buenos Aires","Garin") ,f).getTemperatura().equals("25.76"));
      		   
    }
    @After public void despues()
    {

        cache.borrarTodo();
    }
 
}
