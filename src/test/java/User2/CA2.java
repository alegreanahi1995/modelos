package User2;

import org.apache.http.client.ClientProtocolException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import modelo.Clima;
import modelo.ControladordeServicio;
import modelo.Fecha;
import modelo.Lugar;
import modelo.NullClima;
import modelo.ServicioClima;
import servicios.ServicioPrueba;
import servicios.ServicioPruebaNoFunciona;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
public class  CA2 {


	ControladordeServicio contro_servicios=new ControladordeServicio();
	   

    @Test public void Consulta_Proveedor_NoContiene() throws ClientProtocolException, URISyntaxException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {


    	 List<ServicioClima> lista=new ArrayList<ServicioClima>();
  	   
	    lista.add(new ServicioPruebaNoFunciona());
	    contro_servicios.cargarServicios(lista);
	    Lugar lugar=new Lugar("Argentina","Buenos Aires","Garin");
	    

		Fecha f=new Fecha(02,06,2020);
	    Clima clima=contro_servicios.getServicios().get(0).getTiempo(lugar, f);
	  
	    assertTrue(clima instanceof NullClima);
	   
    }




}
