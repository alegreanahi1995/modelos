package User1;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import modelo.ControladordeServicio;
import modelo.Fecha;
import modelo.Lugar;
import modelo.ServicioClima;
import servicios.ServicioPrueba;
import servicios.ServicioPruebaNoFunciona;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.http.client.ClientProtocolException;
public class CA4{

    @Test public void Consulta_proveedoresfuncionanynofuncionan() throws ClientProtocolException, URISyntaxException, IOException, InterruptedException, InstantiationException, IllegalAccessException, ClassNotFoundException {
    	ControladordeServicio contro_servicios= new ControladordeServicio();
 	   
    List<ServicioClima> lista=new ArrayList<ServicioClima>();
  	   	
    lista.add(new ServicioPrueba());
    lista.add(new ServicioPruebaNoFunciona());
    lista.add(new ServicioPrueba());
    lista.add(new ServicioPruebaNoFunciona());
    contro_servicios.cargarServicios(lista);
    assertTrue(contro_servicios.serviciosQueFuncionan().size()==2);

    Lugar lugar=new Lugar("Argentina","Buenos Aires","Garin");
    
    Calendar hoy=Calendar.getInstance();
		Date fechaactual=new Date();
	SimpleDateFormat simple=new SimpleDateFormat("dd/MM/YYYY");
	String fechastring=simple.format(fechaactual);
	

	String[] fecha = fechastring.split("/");
	int dia = Integer.parseInt(fecha[0].toString()); 
	int mes = Integer.parseInt(fecha[1].toString()); 
	int anio=Integer.parseInt(fecha[2].toString()); 

	hoy.set(anio, mes,dia);
	
	Fecha f=new Fecha(dia,mes,anio);
    assertTrue(  contro_servicios.primerServicioFuncionayContiene(lugar, f) instanceof ServicioPrueba );
    
    
   
}

	
}
