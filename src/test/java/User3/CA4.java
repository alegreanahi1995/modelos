package User3;


import org.apache.http.client.ClientProtocolException;
import org.junit.After;
import org.junit.Test;

import modelo.Cache;
import modelo.Clima;
import modelo.ControladorDeCache;
import modelo.Fecha;
import modelo.Lugar;
import modelo.NullClima;
import modelo.PoliticaCache1;
import modelo.RealClima;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
public class  CA4 {


	  Cache cache=Cache.getInstancia();
  Fecha f= new Fecha(10,4,2021);
  ControladorDeCache cont_cache=new ControladorDeCache(new PoliticaCache1());
  
  @Test public void Cache_Agregar() throws ClientProtocolException, URISyntaxException, IOException {
	  Lugar l=new Lugar( "Argentina","Buenos Aires","Garin");
	  
      assertTrue(Cache.getInstancia().tamanio()==0);
      Clima t=new RealClima(f, l, "25.76", "25.76", "light rain");
      String cacheKey=l.getPais()+l.getProvincia()+l.getLocalidad()+"|"+f.getAnio()+f.getMes()+f.getDia();
   	  
      Calendar hoy=Calendar.getInstance();
		Date fechaactual=new Date();
		SimpleDateFormat simple=new SimpleDateFormat("dd/MM/YYYY");
		String fechastring=simple.format(fechaactual);
		

		String[] fecha = fechastring.split("/");
		int dia = Integer.parseInt(fecha[0].toString()); 
		int mes = Integer.parseInt(fecha[1].toString()); 
		int anio=Integer.parseInt(fecha[2].toString()); 

		
		f=new Fecha(dia,mes,anio);
		
      cont_cache.agregar(cacheKey, t);
          
         Clima tiempodevuelto=(Clima) cache.devolverValor(cacheKey);
         
         assertTrue(tiempodevuelto.getTemperatura()==t.getTemperatura());

         assertTrue(tiempodevuelto.getMaxTemperatura()==t.getMaxTemperatura());

         assertTrue(tiempodevuelto.getCondiciones()==t.getCondiciones());
         
    		  
    }
    
    @After public void despues()
    {

		  cache.borrarTodo();
    }
}
