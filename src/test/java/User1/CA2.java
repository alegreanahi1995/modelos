package User1;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import modelo.ControladordeServicio;
import modelo.Lugar;
import modelo.ServicioClima;
import servicios.ServicioPrueba;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.http.client.ClientProtocolException;
public class CA2{

	
	    @Test public void Consulta_Proveedor_Unico_Funciona() throws ClientProtocolException, URISyntaxException, IOException, InterruptedException, InstantiationException, IllegalAccessException, ClassNotFoundException {

	    List<ServicioClima> lista=new ArrayList<ServicioClima>();
	    ControladordeServicio contro_servicios= new ControladordeServicio();
	    	
	    lista.add(new ServicioPrueba());
	    contro_servicios.cargarServicios(lista);
	    assertTrue(contro_servicios.getServicios().size()==1);
	    assertTrue(contro_servicios.getServicios().get(0).getEstado().compareTo("Con conexion")==0);

	   
}
 

}
