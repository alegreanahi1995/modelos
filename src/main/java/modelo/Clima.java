package modelo;

public interface Clima {

		

		public abstract String getCondiciones();
		public abstract Lugar getLugar() ;
		public abstract String toString();

		public String getMaxTemperatura() ;
		public String getTemperatura();
		
}
