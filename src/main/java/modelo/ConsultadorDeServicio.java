package modelo;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Calendar;

import org.apache.http.client.ClientProtocolException;

public class ConsultadorDeServicio implements ConsultadorClima{

	@Override
	public Clima getTiempo(Lugar l, Fecha f) throws ClientProtocolException, URISyntaxException, IOException {

		ServicioClima serv=(ServicioClima)ControladordeServicio.primerServicioFuncionayContiene(l,f);
		
		if(serv instanceof ServicioNull)
		{

			return new NullClima();
		}
		else
		{

			return serv.getTiempo(l, f);
		}
	}

}
