package modelo;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Calendar;

import org.apache.http.client.ClientProtocolException;

public class Meteorologo {

	ControladordeServicio controservicio;
	ConsultadorClima consulclima;
	
	public Meteorologo(String ruta)
	{
		controservicio=new ControladordeServicio(ruta);
		consulclima=new ConsultadorDeCache(new ConsultadorDeServicio());
	}

	public Clima getTiempo(Lugar l, Fecha fecha) throws URISyntaxException, IOException 
	{
		
		return consulclima.getTiempo(l, fecha);
	}

	
}
